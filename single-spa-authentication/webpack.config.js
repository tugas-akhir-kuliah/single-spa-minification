const { merge } = require("webpack-merge");
const path = require("path");
const singleSpaDefaults = require("webpack-config-single-spa-react");
const TerserPlugin = require("terser-webpack-plugin");

module.exports = (webpackConfigEnv, argv) => {
  const defaultConfig = singleSpaDefaults({
    orgName: "tuwien",
    projectName: "authentication",
    webpackConfigEnv,
    argv,
  });

  return merge(defaultConfig, {
    optimization: {
      minimize: true,
      minimizer: [
        new TerserPlugin({
          terserOptions: {
            format: {
              comments: false,
            },
            compress: {
              drop_console: true,
            },
          },
          extractComments: false, // remove comments from the output
        }),
      ],
    },
  });
};