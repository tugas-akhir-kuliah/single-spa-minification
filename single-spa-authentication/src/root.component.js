import React from "react";
import { BrowserRouter as Router, Link, Switch, Route } from "react-router-dom";
import Login from "./components/login.component";

export default function Root(props) {
  return (
    <Router>
      <Switch>
        <Route exact path="/authentication/login">
          <Login />
        </Route>
      </Switch>
    </Router>
  );
}
